const { app, BrowserWindow } = require('electron')
const path = require('path');
function createWindow () {
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    icon: path.join(app.getAppPath(), 'mail-files/desktop-icon.png')
  })

  win.setMenu(null);
  win.loadFile('maol-files/content/index.html')
}

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
})

app.whenReady().then(() => {
  createWindow()

  app.on('activate', function () {
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })
})

if(require('electron-squirrel-startup')) return;
