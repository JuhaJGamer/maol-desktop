# MAOL-desktop

MAOL scientific tables desktop browser application. Does NOT come with the files. 

This is effectively a stripped-down quick start project and a few build files,
ready for you to slot in the actual content of the application. 
Would those files run in a server just as well? Yes they would, 
but for everyday use you don't want to be running a server all the time. 

## Building

1. Get the files from somewhere. If you have access to an Abitti ISO, 
   they're located at `/usr/local/share/maol-digi/`. Place them in `maol-files/`,
   so that it contains at least the folder `content`.

2. Do whatever it takes to get `npm` running. I can't remember how you do it.

2. Build with electron-forge: `npm run make`. For a specific platform,
   see `make-mac`, `make-win`, or electron-forge documentation.

## License

It's GPL 3.0 or later. Look at [LICENSE](LICENSE).
